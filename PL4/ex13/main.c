#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>
#include "struct.h"

#define CONSTANT 10
#define P 2

int child(int n){
    int i;
    for (i=0; i<n; i++){
        if(fork()){
            return (i);
        }
    }
     return -1;
}

int main (void){

    structure *st;

    /* verifica criação de memoria */
    int fd = shm_open("/shmtestw12", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
        if(fd < 0) {
            perror("SHM ERROR");
            exit(EXIT_FAILURE);
        }

    /* verifica se tamanho de memoria foi bem criado */
    int t = ftruncate(fd, sizeof(structure));
        if(t < 0){
            perror("FTRUNCATE ERROR");
            exit(EXIT_FAILURE);
        }

    st= (structure*)mmap(NULL, sizeof(structure), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);

    st->cont = 0;
    int i;

    sem_t *sem1 = sem_open("/sem_ex13_first", O_CREAT|O_EXCL,0644,0);
    sem_t *sem2 = sem_open("/sem_ex13_second", O_CREAT|O_EXCL,0644,0);

    int pid = child(P);

    if (pid != -1){
    }
    return 0;

}
