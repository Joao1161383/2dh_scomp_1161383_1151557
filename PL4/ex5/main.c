#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

int main(void){
	sem_t *sem = sem_open("/sem_ex5", O_CREAT|O_EXCL,0644,0);

    if(sem<0){
        perror("ERRO NO SEMAFRO");
        exit(0);
    }
    
    pid_t pid = fork();
     if(pid >0){
        sem_wait(sem);
        printf("--> I'm Father \n");
    }
    else if(pid==0){
        printf("--> I'm the child \n");
        sem_post(sem);
    }
   
    if (sem_unlink("/sem_test") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
return 0;
}
