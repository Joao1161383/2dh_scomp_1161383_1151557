//
//  main.c
//  ex12
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

typedef struct{
    int num;
} struc;

int main(void){
    struc *estrutura;
    sem_t *sem = sem_open("/sem", O_CREAT|O_EXCL,0644,1);
    sem_t *sem2 = sem_open("/sem2", O_CREAT|O_EXCL,0644,0);
    pid_t pid = 0;
    if(sem<0){
        perror("semaphore error");
        exit(EXIT_FAILURE);
    }
    shm_unlink("/shmtest");
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shared mem error");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(struc));
    estrutura = (struc*)mmap(NULL, sizeof(struc), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    int i;
    for(i=0; i<10; i++){
        pid=fork();
        if (pid==0){
            break;
        }
    }
    if(pid==0){
        sem_wait(sem2);
        sleep(1);
        printf("My ticket for Avengers:Endgame is %d\n", estrutura->num);
        sem_post(sem);
        exit(0);
    }
    else if(pid>0){
        time_t t;
        for(i=0; i<10;i++){
            sem_wait(sem);
            sleep(3);
            srand(time(&t));
            estrutura->num = rand()%25;
            sem_post(sem2);
        }
        int munm = munmap(estrutura, sizeof(struc));
        if(munm < 0) {
            printf("unmap error\n");
            exit(EXIT_FAILURE);
        }
        shm_unlink("/shmtest");
        sem_unlink("/sem");
        sem_unlink("/sem2");
    }
}

