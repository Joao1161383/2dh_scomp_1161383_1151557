//
//  main.c
//  Writer
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

typedef struct{
    pid_t pid;
    time_t time;
    int writers;
    int readers;
} process;

int main(void) {
    sem_t *sem = sem_open("/sem", O_CREAT|O_EXCL,0644,1);
    sem_t *sem2 = sem_open("/sem2", O_CREAT|O_EXCL,0644,1);
    //sem_t *sem3 = sem_open("/sem3", O_CREAT|O_EXCL,0644,1);
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shared mem error");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(process));
    process* p = mmap(NULL, sizeof(process), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    int i = 0;
    for (i = 0; i < 10; i++) {
        pid_t pid;
        if((pid = fork())==0){
            sem_wait(sem);
            p->pid = getpid();
            p->time = time(NULL);
            p->writers++;
            sleep(3);
            printf("Number of writers: %d\n", p->writers);
            printf("Number of readers: %d\n", p->readers);
            sem_post(sem);
            sem_post(sem2);
            break;
        }
    }
    wait(NULL);
    munmap(p, sizeof(p));
    //shm_unlink("/shmtest");
    //sem_unlink("/sem");
    //sem_unlink("/sem2");
    //sem_unlink("/sem3");
}
