#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

#define P 3
#define RAND 200

int child(int n){
    int i;
    for (i=0; i<n; i++){
        if(fork()==0){
            return (i);
        }
    }
     return -1;
}


int main(void){
    
    setbuf(stdout,NULL);
    srand(time(NULL));
    int persons= rand() %RAND;
    printf("Total Pessoas: %d",persons);
    
    /*So pode entrar a diferença entre o Max Passageiros e o Nr que esta presente no comboio*/
    int entries = rand() %(RAND - persons);

    int exit=0;
    
    int cont=0;
    
        /*SEMAFOROS*/
    sem_t *sem_entrada = sem_open("/sem_ex11_first", O_CREAT|O_EXCL,0644,entries);
    sem_t *sem_saida = sem_open("/sem_ex11_second", O_CREAT|O_EXCL,0644,persons);
    sem_t *pessoas = sem_open("/sem_ex11_third", O_CREAT|O_EXCL,0644,200);


    int pid = child(P);


            if(pid != -1){

                /*Primeira porta*/
                if (pid == 0){
                    do{
                        printf("Saida Na Porta -- %d --\n", pid+1);
                        sem_getvalue(sem_saida, &exit);
                        /*Se já tiverem saido todos sai*/
                        if(exit == 0){
                            break;
                        }
                        /*se nao manda sair e diz quantos ainda estao presentes*/
                        sem_wait(sem_saida);
                        sem_wait(pessoas);
                        sem_getvalue(sem_saida, &cont);
                        printf("Faltam Sair: %d\n", cont);
                    }while(cont >=0);

                    do{
                        /*manda entrar e diz qunatos estao disponiveis (considera q nao saiu ninguem)*/
                        printf("Entrada na Porta -- %d --\n",pid+1);
                        sem_wait(sem_entrada);
                        sem_post(pessoas);
                        sem_getvalue(sem_entrada, &cont);
                        printf("Faltam Entrar: %d\n",cont);
                    }while (cont>=0);
                }
                if (pid == 1){
                    do{
                        printf("Saida Na Porta -- %d --\n", pid+1);
                        sem_getvalue(sem_saida, &saida);
                        /*Se já tiverem saido todos sai*/
                        if(saida == 0){
                            break;
                        }
                        /*se nao manda sair e diz quantos ainda estao presentes*/
                        sem_wait(sem_saida);
                        sem_wait(pessoas);
                        sem_getvalue(sem_saida, &cont);
                        printf("Faltam Sair: %d\n", cont);
                    }while(cont >=0);

                    do{
                        /*manda entrar e diz qunatos estao disponiveis (considera q nao saiu ninguem)*/
                        printf("Entrada na Porta -- %d --\n",pid);
                        sem_wait(sem_entrada);
                        sem_post(pessoas);
                        sem_getvalue(sem_entrada, &cont);
                        printf("Faltam Entrar: %d\n",cont);
                    }while (cont>=0);
                }if (pid == 2){
                    do{
                        printf("Saida Na Porta -- %d --\n", pid);
                        sem_getvalue(sem_saida, &exit);
                        /*Se já tiverem saido todos sai*/
                        if(exit == 0){
                            break;
                        }
                        /*se nao manda sair e diz quantos ainda estao presentes*/
                        sem_wait(sem_saida);
                        sem_wait(pessoas);
                        sem_getvalue(sem_saida, &cont);
                        printf("Faltam Sair: %d\n", cont);
                    }while(cont >=0);

                    do{
                        /*manda entrar e diz qunatos estao disponiveis (considera q nao saiu ninguem)*/
                        printf("Entrada na Porta -- %d --\n",pid);
                        sem_wait(sem_entrada);
                        sem_post(pessoas);
                        sem_getvalue(sem_entrada, &cont);
                        printf("Faltam Entrar: %d\n",cont);
                    }while (cont>=0);
                }

            }

    if (sem_unlink("/sem_ex11_first") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex11_second") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex11_third") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    
    return 0;
}
