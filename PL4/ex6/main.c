#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

#define NMESSAGES 15

int main(void){

    /*Elimina Buffer*/
    setbuf(stdout,NULL);
    
    sem_t *sem1 = sem_open("/sem_ex6_first", O_CREAT|O_EXCL,0644,1);
    sem_t *sem2 = sem_open("/sem_ex6_second", O_CREAT|O_EXCL,0644,0);
    int i=0;
    
    pid_t p = fork();
    if(p<0){
        printf("Error");
    }else if(p==0){
        for(i=0; i<NMESSAGES; i++){
        sem_wait(sem1);
        printf("I'm the child\n");
        sem_post(sem2);
    }
    }else if(p>0){
        for(i=0; i<NMESSAGES; i++){
            sem_wait(sem2);
            printf("I'm the father\n");
            sem_post(sem1);
        }
    }

    sem_unlink("/sem_ex6_first");
    sem_unlink("/sem_ex6_second");
    return 0;
}
