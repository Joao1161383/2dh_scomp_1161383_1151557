//
//  main.c
//  ex3
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

typedef struct{
    char string[50][80];
    int num;
} struc;

int main(void){
    struc *estrutura;
    sem_t *sem = sem_open("/sem", O_CREAT|O_EXCL,0644,1);
    pid_t pid = 0;
    if(sem<0){
        perror("semaphore error");
        exit(EXIT_FAILURE);
    }
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shared mem error");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(struc));
    estrutura = (struc*)mmap(NULL, sizeof(struc), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    int i;
    for(i=0; i<50; i++){
        pid=fork();
        if (pid==0){
            break;
        }
    }
    if(pid==0){
        char word[80];
        sprintf(word,"I'm the son - With Pid %d\n",getpid());
        sem_wait(sem);
        strncpy(estrutura->string[estrutura->num], word, strlen(word));
        estrutura->num++;
        sem_post(sem);
    }
    else if(pid>0){
        char word[80];
        sprintf(word, "I'm the father - With Pid %d\n",getpid());
        sem_wait(sem);
        strncpy(estrutura->string[estrutura->num], word, strlen(word));
        estrutura->num++;
        sem_post(sem);
        for(i=0; i<51;i++){
         printf("%s",estrutura->string[i]);
         }
        int munm = munmap(estrutura, sizeof(struc));
        if(munm < 0) {
            printf("unmap error\n");
            exit(EXIT_FAILURE);
        }
        shm_unlink("/shmtest");
        sem_unlink("/sem");
    }
}

