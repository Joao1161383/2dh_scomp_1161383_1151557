//
//  main.c
//  ex16
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>
#include "struct.h"

typedef struct{
    int espaco;
    int repeticoes;
}Struc;

int main(void){
    shm_unlink("/shmtest");
    sem_t *sem1 = sem_open("/sem1", O_CREAT|O_EXCL,0644,1);
    sem_t *sem2 = sem_open("/sem2", O_CREAT|O_EXCL,0644,0);
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shared mem error");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(Struc));
    Struc* struc= (Struc*)mmap(NULL, sizeof(struc), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    pid_t pid;
    int turn=0;
    do{
        if((pid = fork())>0){
            sem_wait(sem1);
            printf("East\n");
            sleep(30);
            sem_post(sem2);
        }
        else if(pid == 0){
            sem_wait(sem2);
            printf("West\n");
            sleep(30);
            sem_post(sem1);
        }
        turn++;
    }while(turn!=4);
    sem_unlink("/sem1");
    sem_unlink("/sem2");
    int munm = munmap(struc, sizeof(struc));
    if(munm < 0) {
        printf("unmap failed!");
        exit(EXIT_FAILURE);
    }
    shm_unlink("/shmtest");
    exit(0);
}

