//
//  main.c
//  ex17
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

int main(void){
    
    int i = 0;
    sem_t *sem1 = sem_open("/sem", O_CREAT|O_EXCL,0644,300);
    sem_t *sem2 = sem_open("/sem_vip", O_CREAT|O_EXCL,0644,0);
    sem_t *sem3 = sem_open("/sem_especial", O_CREAT|O_EXCL,0644,0);
    for(i = 0; i < 500; i++){
        pid_t p;
        if((p = fork()) == 0){
            srand(time(NULL) ^ (getpid()<<16));
            int r = (rand() % 3)+1;
            printf("%d\n", r);
            if(r == 1){
                sem_wait(sem1);
                sem_wait(sem3);
                sem_wait(sem2);
                printf("normal\n");
                //sleep(2);
                fflush(stdout);
                sleep(1);
                printf("normal out\n");
                sem_post(sem2);
                sem_post(sem3);
                sem_post(sem1);
                exit(0);
                break;
            }else if(r==2){
                sem_wait(sem3);
                sem_wait(sem2);
                sem_wait(sem1);
                printf("special\n");
                //sleep(2);
                fflush(stdout);
                sleep(1);
                printf("special out\n");
                sem_post(sem2);
                sem_post(sem3);
                sem_post(sem1);
                exit(0);
                break;
            }else if(r==3){
                sem_wait(sem2);
                sem_wait(sem1);
                printf("vip\n");
                //sleep(2);
                sleep(1);
                fflush(stdout);
                printf("vip out\n");
                sem_post(sem2);
                sem_post(sem3);
                sem_post(sem1);
                exit(0);
                break;
            }
            exit(0);
        }
        else
        {
            wait(NULL);
        }
    }
    sem_unlink("/sem");
    sem_unlink("/sem_vip");
    sem_unlink("/sem_especial");
}

