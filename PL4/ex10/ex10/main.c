//
//  main.c
//  ex10
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
    int number;
    char name[30];
    char adress[30];
} user;

int main(void){
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    //sem_t *sem1 = sem_open("/sem1", O_CREAT|O_EXCL,0644,1);
    //sem_t *sem2 = sem_open("/sem2", O_CREAT|O_EXCL,0644,1);
    if(fd < 0) {
        perror("shared mem error");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(user));
    user* data = (user*)mmap(NULL, sizeof(user), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    pid_t p;
    if((p = fork())==0){
        int i = 0, count = 0, option, id = 0;
        do{
            printf("Consult - 1");
            printf("\nInsert - 2");
            printf("\nConsult_All - 3");
            printf("\nExit - 0\n");
            scanf(" %d", &option);
            switch(option)
            {
                case 1:
                    printf("Insert the id you want to check\n");
                    scanf(" %d", &id);
                    int j = 0;
                    for (j=0; j<sizeof(data); j++){
                        if(data[j].number==id){
                            printf("ID:%d\n", data[j].number);
                            printf("NAME:%s\n", data[j].name);
                            printf("ADDRESS:%s\n", data[j].adress);
                        }
                    }
                    i++;
                    break;
                case 2:
                    printf("Insert Personal Data Records\n");
                    printf("Insert an id\n");
                    scanf(" %d", &data[count].number);
                    printf("Insert a name\n");
                    scanf(" %s", data[count].name);
                    printf("Insert an address\n");
                    scanf(" %s", data[count].adress);
                    i++;
                    count++;
                    break;
                case 3:
                    printf("Consult_All\n");
                    for(j=0; j<sizeof(data); j++){
                        if(data[j].number!=0){
                            printf("ID:%d\n", data[j].number);
                            printf("NAME:%s\n", data[j].name);
                            printf("ADDRESS:%s\n", data[j].adress);
                        }
                    }
                    i++;
                    break;
                case 0:
                    exit(0);
                default :
                    printf("Invalid option\n" );
            }
        }while (option != 0 || i < 100);
    }else if(p > 0){
        int status;
        waitpid(-1, &status, 0);
        int m = munmap((void *)data, sizeof(user));
        if(m < 0){
            perror("unmap error");
            exit(EXIT_FAILURE);
        }
        shm_unlink("/shmtest");
        //sem_unlink("/sem1");
        //sem_unlink("/sem2");
    }
}
