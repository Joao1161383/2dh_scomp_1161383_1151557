#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>


#define SONS 8
#define NUMBERS 200

int main(void){

    pid_t pid;
    int i;
    sem_t *sem = sem_open("/sem_ex01", O_CREAT|O_EXCL,0644,1);

    if(sem<0){
        perror("ERRO NO SEMAFRO");
        exit(EXIT_FAILURE);
    }
    for(i=0; i<SONS; i++){
        pid=fork();
        
        
        if(pid==0){
            FILE *file;
            sem_wait(sem);

            int num[NUMBERS];
            
            int start = i*NUMBERS;
            char number[5];

            /*Abrir Ficheiro */
            file=fopen ("Numbers.txt", "r");

            if(file==NULL){
                perror("ERRO FILE");
            }

            int line=0;
            int  linesRead=0;
            int  aux=0;

            while(fgets(number, sizeof(number), file) != NULL && linesRead != 200) {
                if(start == line) {
                    aux = 1;
                }

                if(aux == 1) {
                    num[linesRead] = atoi(number);
                    linesRead++;
                }
                line++;
            }
            fclose(file);

            /* Escrever no Output ("a" -> escreve no mesmo ficheiro ) */
            file = fopen("Output.txt", "a");
            
            if(file == NULL) {
                perror("ERRO FILE OUT\n");
                exit(EXIT_FAILURE);
            }
            int j;
            for(j = 0; j<NUMBERS ; j++) {
                fprintf(file, "%d\n", num[j]);
            }

            fclose(file);

            /*passa o semafro a 1 para prox filho*/
            sem_post(sem);
            exit(0);


        }
    }

    for(i = 0; i < SONS ; i++) {
        wait(NULL);
    }

    /* lê Output */
    FILE *file = fopen("Output.txt", "r");
    char c;

	c = fgetc(file);

	while(c != EOF){
		printf("%c", c);
		c = fgetc(file);
	}
	fclose(file);

	return 0;
    

}
