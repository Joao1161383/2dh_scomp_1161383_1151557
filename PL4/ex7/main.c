#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

#define P 3

int child(int n){
    int i;
    for (i=0; i<n; i++){
        if(fork()==0){
            return (i);
        }
    }
     return -1;
}

int main(void){

    setbuf(stdout,NULL);
    
    /*SEMAFOROS*/
    sem_t *sem1 = sem_open("/sem_ex7_first", O_CREAT|O_EXCL,0644,0);
    sem_t *sem2 = sem_open("/sem_ex7_second", O_CREAT|O_EXCL,0644,0);
    sem_t *sem3 = sem_open("/sem_ex7_third", O_CREAT|O_EXCL,0644,0);
    sem_t *sem4 = sem_open("/sem_ex7_fourth", O_CREAT|O_EXCL,0644,0);
    sem_t *sem5 = sem_open("/sem_ex7_fifth", O_CREAT|O_EXCL,0644,0);
    sem_t *sem6 = sem_open("/sem_ex7_sixth", O_CREAT|O_EXCL,0644,0);
    
    int pid = child(P);


    if(pid!= -1){
        if(pid==0){
            sem_post(sem1);
            printf("Sistemas ");
            sem_post(sem2);
            sem_wait(sem4);
            
            printf(" a ");
            sem_post(sem5);
        }
        if(pid==1){
            sem_wait(sem2);
            
            printf(" de ");
            sem_post(sem3);
            sem_wait(sem5);
            
            printf(" Melhor: ");
            sem_post(sem6);
        }
        if(pid==2){
            sem_wait(sem3);
            
            printf(" Computadores ");
            sem_post(sem4);
            sem_wait(sem6);
            
            printf(" Disciplina. \n");
        }
        exit(0);
    }

    if (sem_unlink("/sem_ex7_first") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex7_second") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex7_third") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex7_fourth") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex7_fifth") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex7_sixth") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    return 0;
}
