#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

#define P 2

int child(int n){
    int i;
    for (i=0; i<n; i++){
        if(fork()==0){
            return (i);
        }
    }
     return -1;
}

int main(void){

    srand(time(NULL));
    setbuf(stdout,NULL);
    /*CRIAÇÂO DE SEMAFOROS*/
    sem_t *semSala = sem_open("/sem_ex15_first", O_CREAT|O_EXCL,0644,0);
    sem_t *semFilaEspera = sem_open("/sem_ex15_second", O_CREAT|O_EXCL,0644,0);
    sem_t *pessoas = sem_open("/sem_ex15_third", O_CREAT|O_EXCL,0644,5);

    int contOut=0;
    int contIn=0;
    int pid = child(P);

    if(pid == -1){
        do {
            /*tempo que ficará dentro ativa a sala*/
            int t = rand() %50; 
            int line = rand()%5;
            printf("Sala de %d minutos!\n", t);
            printf("Fila Espera: %d pessoas\n",line);


            sem_getvalue(semFilaEspera,&cont);
            /*caso haja pessoas a espera remove 1 a um */
            while(contOut > 0){
                sem_wait(semFilaEspera);
            }
            /*passa as pessoas da fila de espera para o semafro da fila e elimina da variavel*/
            while(line>0){
                sem_post(semFilaEspera);
                line--;
            }
            
            sem_post(semSala);
            sleep(t);
            /*passado um determinado tempo elimina a sala*/
            sem_wait(semSala);
            sem_getvalue(pessoas, &contOut);

            /* enquanto a sala nao estiver cheia acrescenta lugares*/
            while (contOut < 5){
                sem_post(pessoas);
                sem_getvalue(pessoas , &contOut);
            }
        }while (1);

    }
        /*Remover Pessoas à Sala*/
        if (pid == 0){
            do{
                sem_getvalue(semSala, &contOut);
                /*se sala estiver criada*/
                if(contOut ==1){
                    /*em diferentes intervalos de tempo vao saindo pessoas*/
                    int interval= rand() %5;
                    
                    sleep(interval);
                    printf("Pessoa Abandonou a Sala\n");
                    sem_wait(pessoas);

                }
            }while(1);
        }
        /*Acrescenta Pessoas à Sala*/
        if(pid == 1){
            do{
                    sem_getvalue(semSala, &contOut);
                    /*se sala estiver criada*/
                    if(contOut==1){
                            sem_getvalue(pessoas, &contIn);
                            /*se houverem menos de 5 pessoas na sala adiciona e remove à fila de espera*/
                            if(contIn < 5){
                                    sem_wait(semFilaEspera);
                                    sem_post(pessoas);
                                    printf("Entrou na Sala\n");
                            }
                    
                    }
            }while (1);
        }
        
     exit(0);
   
     if (sem_unlink("/sem_ex15_first") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex15_second") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex15_third") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    return 0;
}
