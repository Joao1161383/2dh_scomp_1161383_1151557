//
//  main.c
//  ex8
//
//  Created by Élio Machado on 28/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

int main(void){
    sem_t *sem1 = sem_open("/sem1", O_CREAT|O_EXCL,0644,2);
    sem_t *sem2 = sem_open("/sem2", O_CREAT|O_EXCL,0644,2);
    int i=0;
    pid_t p;
    if((p  = fork()) == 0){
        for(i=0; i<10; i++){
            sem_wait(sem1);
            printf("S\n");
            sem_post(sem2);
        }
    }else if(p > 1){
        for(i=0; i<10; i++){
            sem_wait(sem2);
            printf("C\n");
            sem_post(sem1);
        }
    }
    sem_unlink("/sem1");
    sem_unlink("/sem2");
}

