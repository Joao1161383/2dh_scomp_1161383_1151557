#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <semaphore.h>

#define P 2

void buy_chips(){
	printf("Buy Chips\n");
}
void buy_beer(){
	printf("Buy Beer\n");
}
void eat_and_drink(){
	printf("Eat and Drink\n");
}

int child(int n){
    int i;
    for (i=0; i<n; i++){
        if(fork()==0){
            return (i);
        }
    }
     return -1;
}

int main(void){

    /*Elimina Buffer*/
    setbuf(stdout,NULL);
    
    sem_t *sem1 = sem_open("/sem_ex9_first", O_CREAT|O_EXCL,0644,0);
    sem_t *sem2 = sem_open("/sem_ex9_second", O_CREAT|O_EXCL,0644,0);
    sem_t *sem3 = sem_open("/sem_ex9_third", O_CREAT|O_EXCL,0644,0);
    sem_t *sem4 = sem_open("/sem_ex9_fourth", O_CREAT|O_EXCL,0644,0);

    int pid = child(P);

    if(pid!=-1){
        if(pid==0){
            sem_post(sem1);
            
            buy_chips();
            
            sem_post(sem2);
            
            sem_wait(sem3);
            
            eat_and_drink();
            
            sem_post(sem4);
        }
        if (pid==1){
            sem_wait(sem2);
            
            buy_beer();
            
            sem_post(sem3);
            
            sem_wait(sem4);
            
            eat_and_drink();
        }

        exit(0);
    }

    if (sem_unlink("/sem_ex9_first") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex9_second") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex9_third") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    if (sem_unlink("/sem_ex9_fourth") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }

    return 0;
}
