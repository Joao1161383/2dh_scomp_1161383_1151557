//
//  main.c
//  ex8
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#define STR_SIZE 50
#define NR_DISC 10

typedef struct{
    int numero;
    char nome[STR_SIZE];
    int disciplinas[NR_DISC];
    int flag;
} aluno;

int main(void){
    int fd;
    fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shm_open failed!");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(aluno));
    aluno *data = (aluno*)mmap(NULL, sizeof(aluno), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    pid_t p;
    if((p = fork())>0){
        printf("Student name?\n");
        scanf("%20[^\n]", data->nome);//needed to have spaces on name e.g. "ZARA G"
        printf("Student number?\n");
        scanf(" %d", &data->numero);
        int i = 0;
        for(i=0; i<NR_DISC; i++){
            printf("Student grade?\n");
            scanf(" %d", &data->disciplinas[i]);
            if(data->disciplinas[i]>20 || data->disciplinas[i] < 0)
            {
                printf("Invalid grade, grade should be between 0 and 20\n");
                i--; //lazy approach, we accept the data before checking... but i'm lazy
            }
        }
        data->flag=1;
        int m = munmap((void *)data, sizeof(aluno));
        if(m < 0){
            perror("Unmap failed");
            exit(EXIT_FAILURE);
        }
        wait(NULL);
        shm_unlink("/shmtest");
    }else if(p==0){
        while(data->flag==0);
        int low = data->disciplinas[0];
        int high = data->disciplinas[0];
        int i = 0;
        for(i=0; i<NR_DISC; i++){
            if (data->disciplinas[i]<=low){
                low = data->disciplinas[i];
            }
            if(data->disciplinas[i]>=high){
                high = data->disciplinas[i];
            }
        }
        int sum=0;
        for(i=0; i<NR_DISC; i++){
            sum += data->disciplinas[i];
        }
        float average = (float)sum/NR_DISC;
        printf("Best grade was %d\nWorst grade was %d\nAverage grade was %.2f\n", high,low,average);
        exit(0);
    }
}
