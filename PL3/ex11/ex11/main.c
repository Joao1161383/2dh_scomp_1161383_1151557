//
//  main.c
//  ex11
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

//
//  main.c
//  ex10
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

typedef struct{
    int numeros[10];
    int flag_read;
    int flag_write;
} estrutura;

int papi(estrutura *data, int val, int *fd);//too lazy to create a header file
void notpapi(estrutura *data, int *fd);

int main(void){
    shm_unlink("/shmtest");
    int fd[2];
    if(fd < 0) {
        perror("shm_open failed!");
        exit(EXIT_FAILURE);
    }
    pid_t p;
    int numeros[10] = {};
    estrutura datas = {*numeros,0,0};
    estrutura *data = &datas;
    if((p = fork())>0){
        int val = papi(data, 0, fd);
        while(data->flag_write == 0);
        val = papi(data, val, fd);
        while(data->flag_write == 0);
        val = papi(data, val, fd);
        wait(NULL);
        int m = munmap((void *)data, sizeof(estrutura));
        if(m < 0){
            perror("Unmap failed!\n");
            exit(EXIT_FAILURE);
        }
        shm_unlink("/shmtest");
    }else if(p==0){
        notpapi(data,fd);
        close(fd[0]);
        write(fd[1], &data, sizeof(data));
        notpapi(data,fd);
        notpapi(data,fd);
        exit(0);
    }
    return 0;
}

int papi(estrutura *data, int val, int* fd)
{
    int i = 0;
    for(i=0;i<10;i++)
    {
        data->numeros[i] = val++;
    }
    data->flag_read = 1;
    data->flag_write = 0;
    close(fd[0]);
    write(fd[1], &data, sizeof(data));
    return val;
}

void notpapi(estrutura *data, int* fd)
{
    close(fd[1]);
    read(fd[0], &data, sizeof(data));
    while(data->flag_read == 0){
        read(fd[0], &data, sizeof(data));
    }
    close(fd[0]);
    int i = 0;
    for(i=0;i<10;i++)
    {
        printf("Numero %d\n", data->numeros[i]);
    }
    data->flag_read = 0;
    data->flag_write = 1;
}




