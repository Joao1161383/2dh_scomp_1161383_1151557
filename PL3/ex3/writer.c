#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "struct.h"

int main(void){

	int fd = shm_open("/shmtestw2", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
        if(fd < 0) {
            perror("SHM ERROR");
            exit(EXIT_FAILURE);
        }
        
    int t = ftruncate(fd, sizeof(Array));
        if(t < 0){
            perror("FTRUNCATE ERROR");
            exit(EXIT_FAILURE);
        }
        
        
    Array *array;
    array= (Array*)mmap(NULL, sizeof(Array), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    int i;
    for (i=0; i<10; i++){
        array->vetor[i] = rand() % 20;
    }
    
    int r = munmap(array, sizeof(Array));
        if(r < 0) {
            printf("RUNMAP ERROR");
            exit(EXIT_FAILURE);
        }
        
    r = close(fd);
        if(r < 0) {
            printf("CLOSE ERROR (WRITE)");
            exit(EXIT_FAILURE);
        }
        
    
    exit(0);
    return 0;
}
