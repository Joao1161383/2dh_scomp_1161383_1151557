#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "struct.h"



int main (void){

    /* verifica criação de memoria */
    int fd = shm_open("/shmtestw2", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
        if(fd < 0) {
            perror("SHM ERROR");
            exit(EXIT_FAILURE);
        }

    /* verifica se tamanho de memoria foi bem criado */
    int t = ftruncate(fd, sizeof(Array));
        if(t < 0){
            perror("FTRUNCATE ERROR");
            exit(EXIT_FAILURE);
        }



    /* Imprime vetor com 10 posiçoes aleatorias*/
    Array *array;
    array= (Array*)mmap(NULL, sizeof(Array), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    int i=0,aux=0,media=0;
    for (i=0; i<10; i++){
        printf("%d \n", array->vetor[i]);
        aux= aux+array->vetor[i];
    }
    media= aux/10;
    printf("media %d \n", media);

    /* Verifica se existe erro no desmapeamento*/
    int r = munmap(array, sizeof(Array));
        if(r < 0) {
            printf("RUNMAP ERROR");
            exit(EXIT_FAILURE);
        }

    /* Verifica se existe erro no close do write*/
    r = close(fd);
        if(r < 0) {
            printf("CLOSE ERROR (WRITE)");
            exit(EXIT_FAILURE);
        }



    exit(0);
    return 0;
}
