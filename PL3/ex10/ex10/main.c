//
//  main.c
//  ex10
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

typedef struct{
    int numeros[10];
    int flag_read;
    int flag_write;
} estrutura;

int papi(estrutura *data, int val);//too lazy to create a header file
void notpapi(estrutura *data);

int main(void){
    shm_unlink("/shmtest");
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shm_open failed!");
        exit(EXIT_FAILURE);
    }
    ftruncate(fd, sizeof(estrutura));
    estrutura *data = (estrutura*)mmap(NULL, sizeof(estrutura), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
    pid_t p;
    if((p = fork())>0){
        int val = papi(data, 0);
        while(data->flag_write == 0);
        val = papi(data, val);
        while(data->flag_write == 0);
        val = papi(data, val);
        wait(NULL);
        int m = munmap((void *)data, sizeof(estrutura));
        if(m < 0){
            perror("Unmap failed!\n");
            exit(EXIT_FAILURE);
        }
        shm_unlink("/shmtest");
    }else if(p==0){
        while(data->flag_read == 0);
        notpapi(data);
        while(data->flag_read == 0);
        notpapi(data);
        while(data->flag_read == 0);
        notpapi(data);
        exit(0);
    }
    return 0;
}

int papi(estrutura *data, int val)
{
    int i = 0;
    for(i=0;i<10;i++)
    {
        data->numeros[i] = val++;
    }
    data->flag_read = 1;
    data->flag_write = 0;
    return val;
}

void notpapi(estrutura *data)
{
    int i = 0;
    for(i=0;i<10;i++)
    {
        printf("Numero %d\n", data->numeros[i]);
    }
    data->flag_read = 0;
    data->flag_write = 1;
}



