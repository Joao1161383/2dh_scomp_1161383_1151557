#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "struct.h"

#define var 100

int main(void){

    int p1[2];
    pipe(p1);

    Array array;


    int bigVec[1000];
    int i;
    for (i=0; i<1000; i++){
        bigVec[i]= rand() %1000;
        
    }

    /*verificação de erros nos forks*/
    pid_t pid [10];
    for (i = 0; i < 10; i++)
    {
        pid [i] = fork();
        if (pid [i] < 0){
            printf("Erro no fork \n");
        }
        if (pid[i] == 0)
            break;
    }

    if (i== 10){

        close(p1[1]); //fecha write
        int j;
        int bigMax = -1;
        for (j = 0; j < 10; j++){
            
            read(p1[0], &array, sizeof(array));
            if(array.vec[array.id] > bigMax){
                bigMax= array.vec[array.id];
            }

        }
       printf("\nValor maximo total: %d \n", bigMax);
       close(p1[0]);
    }  
    else if (pid[i]==0){
        close(p1[0]);
        int max=0;
        int j;
        for (j = var*i; j< (i*var)+var ; j++){
            if(bigVec[j]>max){
                max=bigVec[j];
            }
        }
        
        array.vec[i]= max;
        array.id=i;
        write(p1[1], &array, sizeof(array));
        printf("Max. encontrado: %d Filho: %d \n", max,i);
        close(p1[1]);
    }



    exit(0);
    return 0;
}
