//
//  main.c
//  ex2
//
//  Created by Élio Machado on 27/04/2019.
//  Copyright © 2019 Élio Machado. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>

#define arrsize 1000000

int main(void){
    struct timeval end, start;
    int array[arrsize],status = 0;
    int i=0;
    for(i=0; i<arrsize; i++){
        array[i] = rand() % 50;
    }
    //shm
    int fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    if(fd < 0) {
        perror("shm_open failed!");
        exit(EXIT_FAILURE);
    }
    int size = ftruncate(fd, sizeof(int)*arrsize);
    if(size < 0){
        perror("failed to define size!");
        exit(EXIT_FAILURE);
    }
    int *data = (int *)mmap(NULL, sizeof(int) * arrsize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    pid_t p1;
    gettimeofday(&start, NULL);
    if((p1 = fork())>0){
        waitpid(p1, &status,0);
        gettimeofday(&end, NULL);
        printf("Shared memory took %d miliseconds\n", (end.tv_usec - start.tv_usec) / 1000);
        int m = munmap((void *)data, sizeof(int) * arrsize);
        if(m < 0){
            perror("unmap error\n");
            exit(EXIT_FAILURE);
        }
        m = shm_unlink("/shmtest");
    }else if(p1 == 0){
        for(i=0; i<arrsize; i++){
            data[i]=array[i];
        }
        exit(0);
    }
    //pipes
    int fd2[2];
    int ret;
    ret = pipe(fd2);
    
    if (ret == -1){
        perror("Error creating pipe");
    }
    pid_t p2;
    gettimeofday(&start, NULL);
    if((p2 = fork())>0){
        close(fd2[1]);
        for(i=0; i< arrsize; i++){
            read(fd2[0], (void *) &array[i], sizeof(int));
        }
        gettimeofday(&end, NULL);
        printf("Pipe took %d miliseconds\n", (end.tv_usec - start.tv_usec)/1000);
    }else if(p2 == 0){
        close(fd2[0]);
        for(i = 0; i < arrsize; i++){
            write(fd2[1], (void *) &array[i], sizeof(int));
        }
        exit(1);
    }
}

