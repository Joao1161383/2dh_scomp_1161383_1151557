#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include "struct.h"


#define aux 100

int main(void){
        Structure *st;

    /* verifica criação de memoria */
    int fd = shm_open("/shmtestw12", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
        if(fd < 0) {
            perror("SHM ERROR");
            exit(EXIT_FAILURE);
        }

    /* verifica se tamanho de memoria foi bem criado */
    int t = ftruncate(fd, sizeof(Structure));
        if(t < 0){
            perror("FTRUNCATE ERROR");
            exit(EXIT_FAILURE);
        }

    st= (Structure*)mmap(NULL, sizeof(Structure), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);

    int i;
    for(i=0; i<10; i++){
        char newPath[50];
        sprintf(newPath, "%d.txt", i);
        puts(newPath);
        strcpy(st->path[i],newPath);
    }
    

   strcpy(st->word, "Teste Palavra");


    pid_t pid[10];
    for(i=0; i<10; i++){

        pid[i]=fork();

        if(pid[i]<0){
            printf("ERROR FORK");
        }
        //se filho sai
        if(pid[i]==0){
            break;
        }
    }

    //percorreu os filhos todos e vai ao pai
    if(i==10){

        int j;
        for(j=0;j<10;j++){
            wait(NULL);
        }
        for(j=0;j<10;j++){
            printf("Ficheiro: %d.txt \n Ocorrencias: %d \n",j,st->ocorrences[j]);
        }

    }
    //se filho:
    else if(pid[i]==0){
            int cont=0;
            char word[100], search[100];
            strcpy(search, st->word);\
            FILE *file;
            char path2[100];
            strcpy(path2, st->path[i]);
            file = fopen(path2, "r");            
        
        if(file==NULL){
                printf ("ERROR file\n");
                exit(0);
        }
        while (fscanf(file ,"%s", word) !=EOF){

           
            if(strcmp (search,word)==0){
                cont++;
            }
        }
        st->ocorrences[i]= cont;

        fclose(file);
        exit(0);
    }




     /* Verifica se existe erro no desmapeamento*/
    int r = munmap(st, sizeof(Structure));
        if(r < 0) {
            printf("RUNMAP ERROR");
            exit(EXIT_FAILURE);
        }

    /*Remove da memoria*/
    if (shm_unlink("/shmtestw12") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }

    exit(0);
    return 0;
}
