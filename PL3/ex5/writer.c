#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "struct.h"

#define var 100

int main(void){

    /* verifica criação de memoria */
    int fd = shm_open("/shmtestw12", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
        if(fd < 0) {
            perror("SHM ERROR");
            exit(EXIT_FAILURE);
        }

    /* verifica se tamanho de memoria foi bem criado */
    int t = ftruncate(fd, sizeof(Array));
        if(t < 0){
            perror("FTRUNCATE ERROR");
            exit(EXIT_FAILURE);
        }
    Array *array;
    array= (Array*)mmap(NULL, sizeof(Array), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);


    /*Criação de vetor com 1000 posicoes aleatorias*/
    int vec[1000];
    
    int i;
    for (i=0; i<1000; i++){
        vec[i]= rand() %1000;
        
    }

    /*verificação de erros nos forks*/
    pid_t pid [10];
    for (i = 0; i < 10; i++)
    {
        pid [i] = fork();
        if (pid [i] < 0){
            printf("Erro \n");
        }
        if (pid[i] == 0)
            break;
    }

    //quando chegar ao fim dos 10 filhos
    if (i== 10){
        int j;
        for (j = 0; j < 10; j++)
        {
            wait(NULL);
        }
        int bigMax = -1;
        for (j = 0; j < 10; j++){
          if (array->vetor[j] > bigMax){
               bigMax = array->vetor[j];
           }
        }
       printf("\nValor maximo total: %d \n", maxMax);
    }  
    //filho
    else if (pid[i]==0){
        int maximum=0;
        int j;
        for (j = var*i; j< (i*var)+var ; j++){
            if(vec[j]>maximum){
                maximum=vec[j];
            }
        }
        array->vetor[i]= maximum;
        printf("Max. encontrado: %d \n", maximum);
        exit (0);
    }
    

    /* Verifica se existe erro no desmapeamento*/
    int r = munmap(array, sizeof(Array));
        if(r < 0) {
            printf("RUNMAP ERROR");
            exit(EXIT_FAILURE);
        }

    /*Remove da memoria*/
    if (shm_unlink("/shmtestw12") < 0)
    {
        perror("shm_unlink()");
        exit(1);
    }
    exit(0);
    return 0;
    
}
