 
 int main() { 
        pid_t pid = fork(); 
        
        if(pid == 0) { 
            fork(); 
            pthread_t thread_id; 
            pthread_create(&thread_id, NULL, thread_func, NULL); 
            pthread_join(thread_id, NULL); 
        } 
        fork(); 
        … 
} 

/* a) 6, 3! = 6
/* b) 10, 6 dos processos existentes +4 dos forks
