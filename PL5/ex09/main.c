#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

#define THREADS 4

#define V1O "A"
#define V1D "C"
#define V2O "A"
#define V2D "D"


typedef struct{
    char origem[5];
    char destino[5];
} Estrutura;

Estrutura estrutura[THREADS];

pthread_mutex_t mutex[3];

/*viagem 1 (A ate C)*/
void* thread_func1(void* args){
    int comboio =* ((int *)args);
    /* Bloqueia A->B */
    pthread_mutex_lock(&mutex[0]); 
    sleep(3);
    printf("\nTempo de Viagem: 30min, Boa Viagem!\n");
    printf("Comboio %d\nOrigem: %s\nDestino: %s\nESTAMOS EM B\n\n", (comboio+1), estrutura[comboio].origem, estrutura[comboio].destino);
    /* Desbloqueia A->B */
    pthread_mutex_unlock(&mutex[0]); 
    
    /* Bloqueia a linha B->C */
    pthread_mutex_lock(&mutex[1]); 
    sleep(3);
    printf("\nTempo de Viagem: 30min, Boa Viagem!\n");
    printf("Comboio %d\nOrigem: %s\nDestino: %s\nESTAMOS EM C\n", (comboio+1), estrutura[comboio].origem, estrutura[comboio].destino);
    printf("Fim de Linha\n\n");
    /* Desbloqueia  B->C */
    pthread_mutex_unlock(&mutex[1]); 
    
    pthread_exit((void *) NULL);
    
}

/* viagem 2 (A ate D) */
void* thread_func2(void* args){
    int comboio =* ((int *)args);
    /* Bloqueia A->B */
    pthread_mutex_lock(&mutex[0]); 
    sleep(3);
    printf("\nTempo de Viagem: 30min, Boa Viagem!\n");
    printf("Comboio %d\nOrigem: %s\nDestino: %s\nESTAMOS EM B\n\n", (comboio+1), estrutura[comboio].origem, estrutura[comboio].destino);
    /* Desbloqueia A->B */
    pthread_mutex_unlock(&mutex[0]); 
    
    /* Bloqueia a linha B->C */
    pthread_mutex_lock(&mutex[2]); 
    sleep(3);
    printf("\nTempo de Viagem: 30min, Boa Viagem!\n");
    printf("Comboio %d\nOrigem: %s\nDestino: %s\nESTAMOS EM D\n", (comboio+1), estrutura[comboio].origem, estrutura[comboio].destino);
    printf("Fim de Linha\n\n");
    /* Desbloqueia  B->C */
    pthread_mutex_unlock(&mutex[2]); 
    
    pthread_exit((void *) NULL);
    
}


int main (void) {
    
    int i;
    int args[THREADS];
    
    pthread_t threads[THREADS];
    
    for(i=0; i<3; i++) {
        int m = pthread_mutex_init(&mutex[i], NULL);
        if(m < 0) {
            perror("MUTEX ERROR");
            exit(EXIT_FAILURE);
        }
    }
    
    for(i=0; i<THREADS; i++) {
        args[i] = i;
        if( i < THREADS/2) {
            strcpy(estrutura[i].origem, V1O);
            strcpy(estrutura[i].destino, V1D);
            pthread_create(&threads[i], NULL, thread_func1, (void*)&args[i]);
        } else {
            strcpy(estrutura[i].origem, V2O);
            strcpy(estrutura[i].destino, V2D);
            pthread_create(&threads[i], NULL, thread_func2, (void*)&args[i]);
        }
    }
    void *retorno;
    pthread_join(threads[0],(void*)&retorno);
    pthread_join(threads[1],(void*)&retorno);
    pthread_join(threads[2],(void*)&retorno);
    pthread_join(threads[3],(void*)&retorno);
    for(i=0; i<3; i++) {
        int d = pthread_mutex_destroy(&mutex[i]);
        if(d < 0) {
            perror("Erro no mutex destroy");
            exit(EXIT_FAILURE);
        }
    }
}
