#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>
#define STR_SIZE 50
#define NUMBER 5

typedef struct{
    int number;
    char name[STR_SIZE];
    char adress[STR_SIZE];
} Pessoa;


void* thread_func(void* arg){

    Pessoa* pessoa = (Pessoa*)arg;
    printf("Number: %d\nName: %s\nAdress: %s\n", pessoa->number, pessoa->name, pessoa->adress);

    pthread_exit((void*) NULL);

}

int main(void){
    Pessoa* s[NUMBER];
    int i=0;
    pthread_t threads[NUMBER];

    for(i=0; i<NUMBER; i++){
        Pessoa* pessoa = malloc(sizeof(Pessoa));

        pessoa->number = i;
        char* nome = "Joao";
		char* rua = "Rua Bacalhau";

        strcpy(pessoa->name, nome);
        strcpy(pessoa->adress, rua);

        s[i]=pessoa;

    }

    for(i=0; i<NUMBER; i++){

        pthread_create(&threads[i] , NULL ,  thread_func ,(void*) s[i]);
		pthread_join(threads[i],(void*)NULL);
    }

    return 0;
}
