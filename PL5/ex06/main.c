#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

typedef struct{
    unsigned int number;
} User;

void* thread_func1(void* arg){
    User* p = (User*)arg;
    unsigned int s=0;
    printf("Insira um valor sem sinal\n");
    scanf("%d", &s);
    p->number=s;
    pthread_exit((void*) NULL);
}

void* thread_func2(void* arg){
    
    User* p = (User*)arg;
    int i=0, j=0, c=0;
    printf("Números primos menores que o número inserido pelo utilizador ou igual\n");
    for(i=2; i<=p->number; i++){
        c=0;
        for(j=1; j<=p->number; j++){
            if(i%j==0){
                c++;
            }
        }
        if(c==2){
            printf("%d\n", i);
        }
    }
    pthread_exit((void*) NULL);
}

int main(void){
    pthread_t thread[2];
    User* m = malloc(sizeof(User));
    pthread_create(&thread[0], NULL ,  thread_func1,(void*)m);
    pthread_join(thread[0],(void*)NULL);
    pthread_create(&thread[1], NULL ,  thread_func2,(void*)m);
    pthread_join(thread[1],(void*)NULL);
}
