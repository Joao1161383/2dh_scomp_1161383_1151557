#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

#define N 10
#define THREADS 5

typedef struct{
    int array[1000];
    int n;
} Estrutura;

void* thread_func1(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    for(i=0;i<200; i++){
        if(estrutura->n==estrutura->array[i]){
            printf("Posição: %d\n",i);
            printf("Thread %u:", pthread_self());  
        }else printf(NULL);   
    }
    pthread_exit((void*) NULL);
}

void* thread_func2(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    for(i=200;i<400; i++){
        if(estrutura->n==estrutura->array[i]){
            printf("Posição: %d\n",i);
            printf("Thread %u:", pthread_self());  
        }else printf(NULL);   
    }
    pthread_exit((void*) NULL);
}

void* thread_func3(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    for(i=400;i<600; i++){
        if(estrutura->n==estrutura->array[i]){
            printf("Posição: %d\n",i);
            printf("Thread %u:", pthread_self());  
        }else printf(NULL);   
        }       
    pthread_exit((void*) NULL);
}

void* thread_func4(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    for(i=600;i<800; i++){
        if(estrutura->n==estrutura->array[i]){
            printf("Posição: %d\n",i);
            printf("Thread %u:", pthread_self());  
        }else printf(NULL);   
    }
    
    pthread_exit((void*) NULL);
}

void* thread_func5(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    for(i=800;i<1000; i++){
        if(estrutura->n==estrutura->array[i]){
            printf("Posição: %d\n",i);
            printf("Thread %u:", pthread_self());  
        }else printf(NULL);   
    }
    pthread_exit((void*) NULL);
}

int main(void){

        Estrutura* estrutura = malloc(sizeof(Estrutura));
        pthread_t threads[THREADS];

        int aux=0;
        printf("Insira um numero: ");
        scanf("%d", &aux);
        estrutura->n=aux;

        int i;
        int j;
        int status;
        srand(time(NULL));

        
        for(i=0;i<1000; i++){
            do{
                    estrutura->array[i]= rand() %1000;
                    status= 1;
                    for(j=0; j<i ;j++){
                        if(estrutura->array[i]==estrutura->array[j]){
                                status=0;
                        }
                    }
            }while(status==0);
        }

       

        pthread_create(&threads[0], NULL ,  thread_func1,(void*)estrutura);
        pthread_join(threads[0],(void*)NULL);
        pthread_create(&threads[1], NULL ,  thread_func2,(void*)estrutura);
        pthread_join(threads[1],(void*)NULL);
        pthread_create(&threads[2], NULL ,  thread_func3,(void*)estrutura);
        pthread_join(threads[2],(void*)NULL);
        pthread_create(&threads[3], NULL ,  thread_func4,(void*)estrutura);
        pthread_join(threads[3],(void*)NULL);
        pthread_create(&threads[4], NULL ,  thread_func5,(void*)estrutura);
        pthread_join(threads[4],(void*)NULL);



return 0;
}
