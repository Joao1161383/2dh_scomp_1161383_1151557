#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

typedef struct{
    int arrayChaves[50000];
    int arrayFinal[50];
} Estrutura;

void* thread_func1(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=0;i<5000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
    }
    pthread_exit((void*) NULL);
}

void* thread_func2(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=5000;i<10000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func3(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=10000;i<15000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func4(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=15000;i<20000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func5(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=20000;i<25000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func6(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=25000;i<30000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func7(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=30000;i<35000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func8(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=35000;i<40000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func9(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=40000;i<45000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux]++;
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func10(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int aux;
    
    for(i=45000;i<50000; i++){
        aux=estrutura->arrayChaves[i];
        estrutura->arrayFinal[aux-1]++;
        
    }
    pthread_exit((void*) NULL);
}



int main(void){
    Estrutura* estrutura = malloc(sizeof(Estrutura));
    pthread_t threads[10];
    srand(time(NULL));
    
    int i;
    for(i=0;i<ARRAY_SIZE*5; i++){
        estrutura->arrayChaves[i]= rand()%49 ;
    }
    
    pthread_create(&threads[0], NULL ,  thread_func1,(void*)estrutura);
    pthread_join(threads[0],(void*)NULL);
    pthread_create(&threads[1], NULL ,  thread_func2,(void*)estrutura);
    pthread_join(threads[1],(void*)NULL);
    pthread_create(&threads[2], NULL ,  thread_func3,(void*)estrutura);
    pthread_join(threads[2],(void*)NULL);
    pthread_create(&threads[3], NULL ,  thread_func4,(void*)estrutura);
    pthread_join(threads[3],(void*)NULL);
    pthread_create(&threads[4], NULL ,  thread_func5,(void*)estrutura);
    pthread_join(threads[4],(void*)NULL);
    pthread_create(&threads[5], NULL ,  thread_func6,(void*)estrutura);
    pthread_join(threads[5],(void*)NULL);
    pthread_create(&threads[6], NULL ,  thread_func7,(void*)estrutura);
    pthread_join(threads[6],(void*)NULL);
    pthread_create(&threads[7], NULL ,  thread_func8,(void*)estrutura);
    pthread_join(threads[7],(void*)NULL);
    pthread_create(&threads[8], NULL ,  thread_func9,(void*)estrutura);
    pthread_join(threads[8],(void*)NULL);
    pthread_create(&threads[9], NULL ,  thread_func10,(void*)estrutura);
    pthread_join(threads[9],(void*)NULL);
    for(i=1;i<50;i++){
        printf("Numero %d: %d vezes\n",i,estrutura->arrayFinal[i]);
    }
}
