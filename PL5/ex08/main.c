#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

typedef struct{
    int data[1000];
    int result[1000];
} Arrays;

void* thread_func1(void* arg){
    Arrays* p = (Arrays*)arg;
    int i=0;
    
    for (i=0; i<200; i++){
        p->result[i]= p->data[i]*2+10;
    }
    
    printf("Primeiros 200 números\n");
    for(i=0; i<200; i++){
        printf("%d  ", p->result[i]);
    }
    printf("\n");
    pthread_exit((void*) NULL);
}

void* thread_func2(void* arg){
    Arrays* p = (Arrays*)arg;
    int i=0;
    for (i=200; i<400; i++){
        p->result[i]= p->data[i]*2+10;
    }
    
    printf("Segundos 200 números\n");
    for(i=200; i<400; i++){
        printf("%d  ", p->result[i]);
    }
    printf("\n");
    pthread_exit((void*) NULL);
}

void* thread_func3(void* arg){
    Arrays* p = (Arrays*)arg;
    int i=0;
    for (i=400; i<600; i++){
        p->result[i]= p->data[i]*2+10;
    }
    
    printf("Terceiros 200 números\n");
    for(i=400; i<600; i++){
        printf("%d  ", p->result[i]);
    }
    printf("\n");
    pthread_exit((void*) NULL);
}

void* thread_func4(void* arg){
    Arrays* p = (Arrays*)arg;
    int i=0;
    for (i=600; i<800; i++){
        p->result[i]= p->data[i]*2+10;
    }
    
    printf("Quartos 200 números\n");
    for(i=600; i<800; i++){
        printf("%d  ", p->result[i]);
    }
    printf("\n");
    pthread_exit((void*) NULL);
}

void* thread_func5(void* arg){
    Arrays* p = (Arrays*)arg;
    int i=0;
    for (i=800; i<1000; i++){
        p->result[i]= p->data[i]*2+10;
    }
    
    printf("Quintos 200 números\n");
    for(i=800; i<1000; i++){
        printf("%d  ", p->result[i]);
    }
    printf("\n");
    pthread_exit((void*) NULL);
}

int main(void){
    pthread_t thread[5];
    int i=0;
    Arrays* m = malloc(sizeof(Arrays));
    for(i=0; i<1000; i++){
        m->data[i]= rand()%10;
    }
    pthread_create(&thread[0], NULL ,  thread_func1,(void*)m);
    pthread_join(thread[0],(void*)NULL);
    pthread_create(&thread[1], NULL ,  thread_func2,(void*)m);
    pthread_join(thread[1],(void*)NULL);
    pthread_create(&thread[2], NULL ,  thread_func3,(void*)m);
    pthread_join(thread[2],(void*)NULL);
    pthread_create(&thread[3], NULL ,  thread_func4,(void*)m);
    pthread_join(thread[3],(void*)NULL);
    pthread_create(&thread[4], NULL ,  thread_func5,(void*)m);
    pthread_join(thread[4],(void*)NULL);
}
