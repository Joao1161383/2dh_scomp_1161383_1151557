#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>

#define THREADS 3

typedef struct{
    int menorSaldo;
    int maiorSaldo;
    int saldoMedio;
    int array[1000];
} Estrutura;


void* thread_func1(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int menor=estrutura->array[0];
    for(i=0;i<1000; i++){
        if(menor>estrutura->array[i]){
            menor=estrutura->array[i];
            estrutura->menorSaldo=menor;
        }
    }
    pthread_exit((void*) NULL);
}


void* thread_func2(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int maior=estrutura->array[0];
    for(i=0;i<1000; i++){
        if(maior<estrutura->array[i]){
            maior=estrutura->array[i];
            estrutura->maiorSaldo=maior;
        }
    }
    pthread_exit((void*) NULL);
}

void* thread_func3(void* arg){
    Estrutura* estrutura = (Estrutura*)arg;
    int i;
    int soma=0;
    for(i=0;i<1000; i++){
        soma=soma+estrutura->array[i];
        estrutura->saldoMedio=soma/ARRAY_SIZE;
    }
    pthread_exit((void*) NULL);
}

int main(void){
    
    Estrutura* estrutura = malloc(sizeof(Estrutura));
    pthread_t threads[THREADS];
    srand(time(NULL));
    
    int i;
    for(i=0;i<1000; i++){
        estrutura->array[i]= rand()%9999 ;
    }
    
    pthread_create(&threads[0], NULL ,  thread_func1,(void*)estrutura);
    pthread_join(threads[0],(void*)NULL);
    pthread_create(&threads[1], NULL ,  thread_func2,(void*)estrutura);
    pthread_join(threads[1],(void*)NULL);
    pthread_create(&threads[2], NULL ,  thread_func3,(void*)estrutura);
    pthread_join(threads[2],(void*)NULL);
    
    printf("Maior Saldo: %d\n",estrutura->maiorSaldo);
    printf("Menor Saldo: %d\n",estrutura->menorSaldo);
    printf("Saldo Medio: %d\n",estrutura->saldoMedio);
}




