#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <pthread.h>
#define NUMBER 5
#define THREADS_NUMBER 7

typedef struct{
    int matriz1[NUMBER][NUMBER];
    int matriz2[NUMBER][NUMBER];
    int matrizfinal[NUMBER][NUMBER];
} Matrizes;

void* thread_func1(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    int i, j;
    for(i=0; i<NUMBER; i++){
        for(j=0; j<NUMBER; j++){
            p->matriz1[i][j] = rand()%10;
        }
    }
    pthread_exit((void*) NULL);
}

void* thread_func2(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    int i, j;
    for(i=0; i<NUMBER; i++){
        for(j=0; j<NUMBER; j++){
            p->matriz2[i][j] = rand()%10;
        }
    }
    pthread_exit((void*) NULL);
}

void* thread_func3(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    
    int i;
    for(i=0; i<NUMBER; i++){
        p->matrizfinal[i][0] = p->matriz1[i][0] * p->matriz2[i][0];
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func4(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    
    int i;
    for(i=0; i<NUMBER; i++){
        p->matrizfinal[i][1] = p->matriz1[i][1] * p->matriz2[i][1];
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func5(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    
    int i;
    for(i=0; i<NUMBER; i++){
        p->matrizfinal[i][2] = p->matriz1[i][2] * p->matriz2[i][2];
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func6(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    
    int i;
    for(i=0; i<NUMBER; i++){
        p->matrizfinal[i][3] = p->matriz1[i][3] * p->matriz2[i][3];
        
    }
    pthread_exit((void*) NULL);
}

void* thread_func7(void* arg){
    
    Matrizes* p = (Matrizes*)arg;
    
    int i;
    for(i=0; i<NUMBER; i++){
        p->matrizfinal[i][4] = p->matriz1[i][4] * p->matriz2[i][4];
        
    }
    pthread_exit((void*) NULL);
}

int main(void){
    
    int i, j;
    
    pthread_t threads[THREADS_NUMBER];
    
    Matrizes* m = malloc(sizeof(Matrizes));

    
    pthread_create(&threads[0] , NULL ,  thread_func1 ,(void*)m);
    pthread_join(threads[0],(void*)NULL);
    pthread_create(&threads[1] , NULL ,  thread_func2 ,(void*)m);
    pthread_join(threads[1],(void*)NULL);

    pthread_create(&threads[2] , NULL ,  thread_func3 ,(void*)m);
    pthread_join(threads[2],(void*)NULL);
    pthread_create(&threads[3] , NULL ,  thread_func4 ,(void*)m);
    pthread_join(threads[3],(void*)NULL);
    pthread_create(&threads[4] , NULL ,  thread_func5 ,(void*)m);
    pthread_join(threads[4],(void*)NULL);
    pthread_create(&threads[5] , NULL ,  thread_func6 ,(void*)m);
    pthread_join(threads[5],(void*)NULL);
    pthread_create(&threads[6] , NULL ,  thread_func7 ,(void*)m);
    pthread_join(threads[6],(void*)NULL);
    
    printf("Matriz final\n");
    for(i=0; i<NUMBER; i++){
        for(j=0; j<NUMBER; j++){
            printf("%d\n", m->matrizfinal[i][j]);
        }
    }
    return 0;
}
