//
//  main.c
//  ex5
//
//  Created by Élio Machado on 04/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void) {
    pid_t p = fork();
    if (p == 0){
        sleep(1);
        exit(1);
    } else {
        int status = -1;
        int status2 = -1;
        pid_t ws = wait(&status); // will return -1
        while(status == -1)
        {
            ws = waitpid(p, &status, 0);//do it again, return status from child will be saved on status
        }
        pid_t p2 = fork();
        if(p2 == 0)
        {
            sleep(2);
            exit(2);
        } else {
            pid_t ws2 = waitpid(p2, &status2, 0); // will return -1
            while(status2 == -1)
            {
                ws2 = waitpid(p2, &status2, 0);//do it again, return status from child will be saved on status
            }
            printf("%d\n",WEXITSTATUS(status));
            printf("%d\n",WEXITSTATUS(status2));
        }
    }
}
