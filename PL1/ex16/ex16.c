#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void my_exec(char *command){
	
	int status;
	pid_t p = fork();
	if(p==0){
		execlp(command, command, "SCOMP", NULL);
	}else {
		waitpid(p, &status, 0);

	}
	int v = WEXITSTATUS(status);
	if(v!=0){
		printf("Erro!");
	}
}
	
int main(){
	
	char *command = "ls";
	//char *command2 = "ps";
	//char *command3 = "who";
	my_exec(command);
	//my_exec(command2);
	//my_exec(command3);
	
	return 0;
}
