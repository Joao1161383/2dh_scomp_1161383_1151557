#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
	int i;
	int cont;
	
	int status;
	
	for(i=0; i<10; i++){
		pid_t p = fork();
		if (p==0){        // caso seja processo filho
			for(cont=(i*100)+1; cont <= (i+1)*100; cont++){
				printf("Número %d ", cont);
			}
			printf("\n");
			printf("Novo processo");
			printf("\n");
			exit(2);
		}else {
		waitpid(p, &status, 0);
		}
	}
	
	return 0;
}
