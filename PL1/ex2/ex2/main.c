//
//  main.c
//  ex2
//
//  Created by Élio Machado on 04/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
int main(void) {
    printf("I’m..\n");
    pid_t p = fork();
    if (p == 0){
        printf("I'll never join you!\n");
    } else {
        wait(NULL);
        printf("the..\n");
        pid_t x = fork();
        if (x == 0){
            printf("I'll never join you!\n");
        } else {
            waitpid(x,0,0);
            //pid do filho, pointer onde guardar a informacao do status do filho(nao e preciso aqui
            // e opcionais tambem nao necessarios
            printf("father!\n");
            p = fork();
            if (p == 0){
                printf("I'll never join you!\n");
            }
        }
    }
}
