//
//  main.c
//  ex7
//
//  Created by Élio Machado on 11/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define ARRAY_SIZE 1000
int
main ()
{
    int numbers[ARRAY_SIZE];    /* array to lookup */
    int n;            /* the number to find */
    time_t t;            /* needed to initialize random number generator (RNG) */
    int i;
    
    /* intializes RNG (srand():stdlib.h; time(): time.h) */
    srand ((unsigned) time (&t));
    
    /* initialize array with random numbers (rand(): stdlib.h) */
    for (i = 0; i < ARRAY_SIZE; i++)
        numbers[i] = rand () % 10000;
    
    /* initialize n */
    n = rand () % 10000;
    
    int status = -1;
    pid_t pid;
    if((pid =fork()) == 0)
    {
        int ret = 0;
        //child
        for(i=0;i<ARRAY_SIZE/2;i++)
        {
            if(numbers[i] == n)
            {
                ret++;
            }
        }
        exit(ret);
    }
    else
    {
        //parent
        int ret = 0;
        for(i=ARRAY_SIZE/2;i<ARRAY_SIZE;i++)
        {
            if(numbers[i] == n)
            {
                ret++;
            }
        }
        while(status == -1)
        {
            waitpid(pid,&status,0);
        }
        printf("%d\n",WEXITSTATUS(status) + ret);
    }
}
