#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main() { 

	int i;
	
	for (i=1; i<4; i++){
		fork();
		execlp("echo","echo", "SCOMP", NULL);
	}
	
}

//a) "SCOMP" aparece 8 vezes
//b) 4 prints estão relacionados com o número de processos filho que irão printar "SCOMP", enquanto que os outros 4 prints
// estão ligados a cada processo pai relativo a cada processo filho
		


