//
//  main.c
//  ex1
//
//  Created by Élio Machado on 03/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void) {
    int x = 1;
    pid_t p = fork();/*pid_t: sys/types.h; fork(): unistd.h*/
    if (p == 0) {
        x = x+1;
        printf("1. x = %d\n", x);
    } else {
        x=x-1;
        printf("2. x = %d\n",x);
    }
    printf("3. %d; x = %d\n", p, x);
}
/**
 a) O processo pai vai executar 2 e 3 e seguidamente o processo filho irá executar 1 e 3
 Seguindo essa lógica, a primeira linha irá imprimir 2. x = 0, a segunda linha irá imprimir o pid do filho
 (1234) e x = 0.
 A 3ª linha irá imprimir x = 2, pois o fork retorna 0 para o filho e ele irá executar x+1 = 2 e a última linha irá imprimir 0 que é o valor de p para o filho e x=2
 **/
/**
 b) Não, a ordem pela qual os processos são executados é aleatória pois estão os dois a executar ao
 mesmo tempo, podendo a ordem mudar se executarmos o programa várias vezes.
 **/
