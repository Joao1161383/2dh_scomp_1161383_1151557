#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int e_prog(char *val){
	int status=0;
	
	pid_t p = fork();
	if(p==0){
		execlp("ls","ls",val,(char*) NULL); //Comando ls
		exit(status);
		
	}else{
		wait(&status);
	}
	
	return WEXITSTATUS(status);
}

int main(){
	
	char f[30];
	printf("Insira um nome de um ficheiro\n");
	scanf("%c\n", f);
	
	char *nome = f;
	int val = e_prog(nome);
	printf("%d\n", val);
	
	return 0;
	
}
