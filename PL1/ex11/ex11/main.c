//
//  main.c
//  ex11
//
//  Created by Élio Machado on 13/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define ARRAY_SIZE 1000

int main() {
    int numbers[ARRAY_SIZE];    /* array to lookup */
    int result[ARRAY_SIZE];
    time_t t;            /* needed to initialize random number generator (RNG) */
    int i;
    int j;
    
    /* intializes RNG (srand():stdlib.h; time(): time.h) */
    srand ((unsigned) time (&t));
    
    /* initialize array with random numbers (rand(): stdlib.h) */
    for (i = 0; i < ARRAY_SIZE; i++)
        numbers[i] = rand () % 256;
    
    int status[5] = {-1,-1,-1,-1,-1};
    int maxes[5] = {};
    int max = 0;
    pid_t pid;
    for(i=0;i<5;i++)
    {
        if((pid =fork()) == 0)
        {
            //child
            for(j=200*i;j<(200*i)+200;j++)
            {
                if(numbers[j] > max)
                {
                    max = numbers[j];
                }
            }
            exit(max);
        }
        else
        {
            while (status[i] == -1) {
                waitpid(pid, &status[i], 0);
            }
            maxes[i] = WEXITSTATUS(status[i]);
        }
    }
    int max1 = maxes[0];
    for(i=1;i<5;i++)
    {
        if(maxes[i] > max1)
        {
            max1 = maxes[i];
        }
    }
    max = -1; //reusing variable cuz why waste space?
    if((pid =fork()) == 0)
    {
        for (i = 0; i < ARRAY_SIZE/2; i++) {
            result[i] = ((int) numbers[i] / max1)*100;
            printf("FILHO %d\n", result[i]);
        }
    }
    else
    {
        for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {
            result[i] = ((int) numbers[i] / max1)*100;
        }
        while (max == -1){
            waitpid(pid, &max, 0);
        }
        for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {
            printf("PAI %d\n", result[i]);
        }
    }
}
