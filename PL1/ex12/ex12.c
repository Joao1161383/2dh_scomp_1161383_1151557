#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void spawn_childs(int n){

	int aux = 2;
	int i, status;
	
	for(i = 0; i<n; i++){
		pid_t p = fork();
			if(p==0){
				printf("%d\n", (i+1)*aux);
				exit(status);
			}
			else{
				waitpid(p, &status, 0);
			}
		}
		
		printf("Pai 0\n");
}

int main(){
	
	int nFilhos = 6;
	
	spawn_childs(nFilhos);
	
	return 0;
}
