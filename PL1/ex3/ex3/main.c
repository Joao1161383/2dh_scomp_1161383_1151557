//
//  main.c
//  ex3
//
//  Created by Élio Machado on 04/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>

int main(void) {
    fork(); fork(); fork();
    printf("SCOMP!\n");
}
/**
 a) sao criados 7 processos
 o processo pai imprime 1 vez scomp e cria 3 processos filho = 4 processos
 o primeiro processo filho cria outros 2 processos filhos 4+2 = 6
 o segundo processo filho cria outro processo 6+1 = 7
 **/

/**
 b) respondido acima
 **/

/**
 c) 7, 1 vez por processo
 **/
