//
//  main.c
//  ex8
//
//  Created by Élio Machado on 11/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    pid_t p;
    
    if (fork() == 0) {
        printf("PID = %d\n", getpid()); exit(0);
    }
    
    if ((p=fork()) == 0) {
        printf("PID = %d\n", getpid()); exit(0);
    }
    
    printf("Parent PID = %d\n", getpid());
    
    printf("Waiting... (for PID=%d)\n",p);
    waitpid(p, NULL, 0);
    
    printf("Enter Loop...\n");
    while (1); /* Infinite loop */
}
/*
 a) Yes. The child enters zombie mode while the parent is still running because it has an exit
 status that has to be returned to the parent. While(1) is an infinite loop, so all 3 are still running
 */
/*
 b) Yes. It uses no cpu and no memory.
 */
/*
 c) Program ends and all 3 processes terminate.
 */
