//
//  main.c
//  ex10
//
//  Created by Élio Machado on 13/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define ARRAY_SIZE 2000

int main() {
    int numbers[ARRAY_SIZE];    /* array to lookup */
    int n;            /* the number to find */
    time_t t;            /* needed to initialize random number generator (RNG) */
    int i;
    int j;
    
    /* intializes RNG (srand():stdlib.h; time(): time.h) */
    srand ((unsigned) time (&t));
    
    /* initialize array with random numbers (rand(): stdlib.h) */
    for (i = 0; i < ARRAY_SIZE; i++)
        numbers[i] = rand () % 10;
    
    /* initialize n */
    n = rand () % 100;
    
    int status[10] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
    pid_t pid;
    for(i=0;i<10;i++)
    {
        if((pid =fork()) == 0)
        {
            //child
            for(j=200*i;j<(200*i)+200;j++)
            {
                if(numbers[j] == n)
                {
                    exit(j);
                }
            }
            exit(255);
            break;
        }
        else
        {
            while (status[i] == -1) {
                waitpid(pid, &status[i], 0);
            }
            if(WEXITSTATUS(status[i])!= 255)
            {
                printf("FOUND N ON %d\n", 200*i+WEXITSTATUS(status[i]));
            }
        }
    }
}
