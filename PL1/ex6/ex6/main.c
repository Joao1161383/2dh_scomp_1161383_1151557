//
//  main.c
//  ex6
//
//  Created by Élio Machado on 07/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(){//needs to be main to have a ret
    int i;
    int status;
    pid_t pid = getpid();
    pid_t sonpid;
    for (i = 0; i < 4; i++) {
        if ((sonpid = fork())== 0) {//will only fork 4 times
            break;//b
        }
        {   //c
            if(sonpid % 2 == 0)
            {
                waitpid(sonpid,NULL,0);
                printf("Waiting for %d\n",sonpid);
            }
            else
            {
                printf("Not waiting for %d\n",sonpid);
            }
        }
    }
    if(getpid() != pid)//d
    {
        return i+1;
    }
    printf("This is the end.\n") ;
}

/*
 a) This will create 15 processes 1+3+2+2+1+1+2+1+1+1
 */
/*
 b) instead of sleep, break, if the child process gets created, it'll exit the loop
 */

