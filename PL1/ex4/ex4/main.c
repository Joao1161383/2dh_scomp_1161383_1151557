//
//  main.c
//  ex4
//
//  Created by Élio Machado on 04/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>

int main(void) {
    int a=0, b, c, d;
    b = (int) fork();
    c = (int) getpid(); /*getpid(), getppid(): unistd.h*/
    d = (int) getppid();
    a=a+5;
    printf("\na=%d, b=%d, c=%d, d=%d\n",a,b,c,d);
}

/**
 a) a irá ser sempre 5 em ambos os processos
 b do processo pai irá ser igual ao c do processo filho ( b retorna o pid do processo filho no fork e c no filho retorna o pid do filho)
 c do processo pai será sempre igual ao d do processo filho ( c retorna o pid do processo pai e d retorna o parent pid do filho que é o do pai
 **/
/**
 b) 
 **/
