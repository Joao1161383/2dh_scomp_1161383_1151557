//
//  main.c
//  ex17
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    if(strcmp("root",argv[1]) == 0)
    {
        if(strcmp("alpine",argv[2]) == 0)
        {
            exit(2);
        }
        exit(1);
    }
    exit(0);
}
