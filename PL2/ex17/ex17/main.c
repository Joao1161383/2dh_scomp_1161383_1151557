//
//  main.c
//  ex17
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char * argv[]) {
    char* input= "/Users/eliomachado/Documents/2dh_scomp_1161383_1151557/PL2/ex17/valid";
    int fd[2];
    char id[256];
    char pw[256];
    if(pipe(fd) == -1)
    {
        perror("Failed to create pipe!");
    }
    pid_t pid;
    int status = -1;
    printf("id:");
    scanf("%s",id);
    printf("pw:");
    scanf("%s",pw);
    char * argv1[] = {input, id , pw };
    if((pid = fork()) == 0)
    {
        execve(argv1[0],argv1,NULL);
    }
    else
    {
        while(status == -1)
        {
            waitpid(-1, &status, 0);
        }
        if (WEXITSTATUS(status) == 2) {
           printf("PASSWORD VERIFIED\n");
        }
        else if (WEXITSTATUS(status) == 1) {
            printf("INVALID PASSWORD\n");
        }
        else if (WEXITSTATUS(status) == 0) {
            printf("NO SUCH USER\n");
        }
    }
}
