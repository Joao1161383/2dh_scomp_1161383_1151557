//
//  main.c
//  ex4
//
//  Created by Élio Machado on 23/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int fd[2];
int ret;
int main(){
    char text[1000];
    FILE *fp=fopen("/Users/eliomachado/Documents/2dh_scomp_1161383_1151557/PL2/ex4/ex4/brattysis.txt", "r");
    fgets(text,1000,fp);
    ret = pipe(fd);
    if (ret==-1){
        printf("Error creating pipe \n");
    }
    pid_t pid;
    if((pid =fork()) == 0)
    {
        close(fd[1]);
        read(fd[0], text, 1000);
        printf("%s\n", text);
        close(fd[0]);
    }
    else {
        close(fd[0]);
        write(fd[1], text, 1000);
        close(fd[1]);
    }
}


