//
//  main.c
//  ex2
//
//  Created by Élio Machado on 23/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void nostruc(void);
void struc(void);
int fd[2];
int ret, status;
char nome[256];
int length = 0;
int n;
int main(){
    printf("string:");
    fgets (nome, 256, stdin);
    int i;
    for(i = 0 ; i < 257; i++)
    {
        if (nome[i] == '\0') {
            length = i - 1;
            break;
        }
    }
    printf("numero:");
    scanf ("%d", &n);
    ret = pipe(fd);
    if (ret==-1){
        printf("Error creating pipe \n");
    }
    printf("With structure?: S/N \n");
    char x[1];
    scanf (" %c", &x[0]); //\n is still in buffer, so we put a space to consume it so we can read
    if (x[0] == 'S') {
        struc();
    }
    else if (x[0] == 'N')
    {
        nostruc();
    }
    exit(0);
}

void nostruc()
{
    pid_t pid;
    if((pid =fork()) == 0)
    {
        close(fd[1]);
        read(fd[0], &n, sizeof(int));
        printf("%d\n", n);
        read(fd[0], nome, length);
        printf("%s", nome);
        close(fd[0]);
    }else {
        close(fd[0]);
        write(fd[1], &n, sizeof(int));
        write(fd[1], nome, length);
        close(fd[1]);
        waitpid(pid, &status,0);
    }
}

void struc()
{
    struct estrutura{
        char nome[256];
        int n;
    }strc,*ptr;

    ptr = &strc;
    strc.n = n;
    strcpy(strc.nome, nome);
    
    if (ret == -1){
        printf("Error creating pipe \n");
    }
    
    pid_t pid;
    if((pid =fork()) == 0)
    {
        struct estrutura strc2, *ptr2;
        ptr2 = &strc2;
        close(fd[1]);
        read(fd[0], ptr2, sizeof(strc2));
        printf("%s", ptr2->nome);
        printf("%d\n", ptr2->n);
        close(fd[0]);
    }else {
        close(fd[0]);
        write(fd[1], ptr, sizeof(strc));
        close(fd[1]);
        waitpid(pid, &status,0);
    }
}
