#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME 30
#define L 5

typedef struct
{
	char nome[NAME];
	int preco;
} reader;

int main(){
	
	int fd[2];
	int i;
	
	pid_t p[L];
	
	if(pipe(fd) == -1){
		printf("Erro no pipe");
		return 1;
	}
	
	for(i = 0; i < L; i++){
		p[i] = fork();
		if(p[i] == 0){
			break;
		}
	}
	
	if(i==L){ //Concluido o for anterior, i=5
		reader r1;
		int j;
		close(fd[0]);
		
		for(j=0; j<L; j++){
			strcpy(r1.nome, "Almofada");
			r1.preco = 86;
			write(fd[1], &r1, sizeof(reader));
		}
		close(fd[1]);
	
	}else if(p[i] == 0){
		reader r2;
		close(fd[1]);
		read(fd[0], &r2, sizeof(reader));
		printf("\nNome: %s, Preco: %d \n", r2.nome, r2.preco);
		close(fd[0]);
		exit(2);
	}
	
	return 0;
}
