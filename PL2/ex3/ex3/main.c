//
//  main.c
//  ex3
//
//  Created by Élio Machado on 23/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#define BUFFER_SIZE 15


int main()
{
    int fd[2];
    int status;
    char read_msg [BUFFER_SIZE];
    char write_msg [BUFFER_SIZE] = "Hello World";
    char write_msg2 [BUFFER_SIZE] = "Goodbye";
    
    if(pipe(fd)== -1){
        perror("Error creating pipe! \n");
    }
    pid_t pid;
    if((pid =fork()) == 0){
        close(fd[1]);
        read(fd[0], read_msg, BUFFER_SIZE);
        printf("Filho: %s\n", read_msg);
        read(fd[0], read_msg, BUFFER_SIZE);
        printf("Filho: %s\n", read_msg);
        close(fd[0]);
    }
    else{
        close (fd[0]);
        write(fd[1], write_msg, BUFFER_SIZE);
        write(fd[1], write_msg2, BUFFER_SIZE);
        close (fd[1]);
        waitpid(pid,&status,0);
        printf("pid filho: %d %d\n",pid,WEXITSTATUS(status));
    }
}
