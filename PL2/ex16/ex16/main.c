//
//  main.c
//  ex16
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    int fd[2];
    char buff[256];
    if(pipe(fd) == -1)
    {
        perror("Failed to create pipe!");
    }
    pid_t pid;
    if((pid = fork()) == 0)
    {
        dup2(fd[1], STDOUT_FILENO);//redirect output to pipe
        close(fd[0]);
        close(fd[1]);
        execl("/bin/sh" , "sh", "-c", "ls -la | sort | wc -l", NULL);
    }
    else
    {
        wait(NULL);
        close(fd[1]);
        read(fd[0], buff, sizeof(buff));
        printf("%s",buff);
    }
}
