#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 1000

int main(){
	
	int a1[SIZE];
	int a2[SIZE];
	
	int i;
	
	for(i=0; i<SIZE; i++){
		a1[i] = rand()%500;
	}
	for(i=0; i<SIZE; i++){
		a2[i] = rand()%500;
	}
	
	int status, j;
	int div = SIZE/5;
	int aux = 0;
	int resultado = 0;
	int fd[2];
	int val = pipe(fd);
	
	if(val == -1){
		printf("Erro no pipe");
	}
	
	pid_t p;
	for(i=0; i<5; i++){
		p = fork();
		if(p==0){
			for(j=i*div; j<(i+1)*div; j++){
				aux = a1[j] + a2[j];
			}
			
			close(fd[0]);
			write(fd[1], &aux, sizeof(int));
			close(fd[1]);
			exit(2);
		}
	}
	
	if(p>0){
		waitpid(p, &status, 0);
		close(fd[1]);
		read(fd[0], &aux, sizeof(int));
		resultado += aux;
		read(fd[0], &aux, sizeof(int));
		resultado += aux;
		read(fd[0], &aux, sizeof(int));
		resultado += aux;
		read(fd[0], &aux, sizeof(int));
		resultado += aux;
		read(fd[0], &aux, sizeof(int));
		resultado += aux;
		close(fd[0]);
	}
	
	printf("%d\n", resultado);
	
	return 0;
}
	
