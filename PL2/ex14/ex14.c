#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

int main(){
	
	char info[40];
	int i;
	
	int fd[2];
	
	if(pipe(fd) < 0){
		printf("Erro a criar pipe");
		return 1;
	}
	
	pid_t p = fork();
	if(p == 0){
		close(fd[1]);
		dup2(fd[0], 0);
		close(fd[0]);
		execlp("more","more", (char*)NULL);
		exit(2);
	}else if(p > 0){
		close(fd[0]);
		for(i = 0; i < 100; i++){
			sprintf(info, "Line %d", i);
			strcat(info, "\n");
			write(fd[1], info, strlen(info));
		}
		close(fd[1]);
		wait(NULL);
	}
	return 0;
}
	
