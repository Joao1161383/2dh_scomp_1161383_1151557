//
//  main.c
//  ex9
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
# define BUFFER_SIZE 50000

int main(){
    int status, i , j;
    time_t t;
    int products[50000];
    int fd[2];
    pipe(fd);
    pid_t x;
    
    typedef struct{
        int customer_code;
        int product_code;
        int quantity;
    }Estrutura;
    
    Estrutura es[BUFFER_SIZE];
    
    for(i=0;i<BUFFER_SIZE-1;i++){
        es[i].customer_code=0;
        es[i].product_code=random () % 501;
        es[i].quantity=random () % 21;
    }
    
    for (i=0; i<10; i++){
        x= fork();
        int h[5000];
        int hx = 0;
        if(x==0){
            for(j=(i*5000); j<(i+1)+5000; j++){
                if(es[j].quantity>=20){
                    h[hx] = es[j].product_code;
                    hx++;
                }
            }
            close(fd[0]);
            write(fd[1], &h, sizeof(h));
            close(fd[1]);
            break;
        }
        else
        {
            int k;
            wait(NULL);
            close (fd[1]);
            read(fd[0], &k, sizeof(j));
            printf("%d\n",k);
            close (fd[0]);
        }
    }
    //waitpid (x, &status, 0);
    //printf("Final: %d\n",esfinal[BUFFER_SIZE].product_code);
}
