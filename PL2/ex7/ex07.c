#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <time.h>

#define SIZE 1000
#define PROCESS 5

int main(void){
	
	int a1[SIZE];
	int a2[SIZE];
	int result[SIZE];
	int i;
	
	for(i = 0; i < SIZE; i++){
		a1[i] = rand() %100;
	}
	for(i = 0; i < SIZE; i++){
		a2[i] = rand() %100;
	}
	
	int status, j, sum;
	int fd[PROCESS][2];
	
	for(i = 0; i < PROCESS; i++){
		if(pipe(fd[i]) == -1){
			perror("Erro a criar pipe\n");
			exit(1);
		}
		
		for(i = 0; i < PROCESS; i++){
			pid_t p = fork();
			if(p==0){
				for(i = 0; i < PROCESS; i++){
					close(fd[i][0]);
					for(j=(i*200); j<(i+1)*200; j++){
						sum = a1[j] + a2[j];
						result[j] = sum;
						write(fd[i][1], &sum, sizeof(int));
					}
				
					close(fd[i][1]);
				}
			}else {
				waitpid(p, &status, 0);
				
				for(i=0; i<SIZE; i++){
					for(j=0;j<PROCESS;j++){
						close(fd[j][1]);
						read(fd[j][0], &sum, sizeof(int));
						result[j] = sum;
						close(fd[j][0]);
					}
				}
			}
		
			printf("\n");
			printf("\n");
		
			for(i=0; i<SIZE; i++){
				printf(" %d+%d = %d\n", a1[i], a2[i], result[i]);
			}
		}
	}
	return 0;
}
