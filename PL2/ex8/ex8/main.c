//
//  main.c
//  ex8
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void){
    
    int i;
    int fd[2];
    
    if(pipe(fd) == -1){
        perror("Error creating pipe!\n");
        return 1;
    }
    
    typedef struct{
        int ronda;
        char win[10];
    } strc;
    
    pid_t p[10];
    pid_t ppid = getpid();
    for(i = 0; i < 10; i++){
        p[i] = fork();
        if(p[i] == 0){
            break;
        }
    }
    strc struc[10];
    for (i = 0; i < 10; i++) {
        if(getpid() == ppid)
        {
            sleep(2);
            struc[i].ronda = i+1;
            strcpy(struc[i].win, "Win");
            close(fd[0]);
            write(fd[1], &struc[i], sizeof(struc[i]));
        }
        else
        {
            close(fd[1]);
            read(fd[0], &struc[i], sizeof(struc[i]));
            close(fd[0]);
            if(struc[i].ronda > 0 && struc[i].ronda < 11)
            {
                pid_t pid = getpid();
                printf("%s %d %d\n",struc[i].win,struc[i].ronda,pid);
                exit(struc[i].ronda);
            }
        }
    }
    return 0;
}

