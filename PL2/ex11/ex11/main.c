//
//  main.c
//  ex11
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

int main() {
    
    int i,n;
    pid_t pid;
    int p1[2];
    int p2[2];
    time_t t;
    pid_t ppid = getpid();
    int z;
    if (pipe(p1) == -1)
    {
        perror("Error creating pipe\n");
        return 1;
    }
    
    for(i = 0; i < 5; i++)
    {
        pipe(p2);
        fflush(stdout);
        if((pid = fork()) == 0)
        {
            int helper;
            //randomness
            pid_t pids = getpid();
            srand ((unsigned) time (&t)^pids);
            n = rand () % 501;
            printf("Number : %d, PID : %d \n",n,pids);
            //magic
            close(p1[1]);
            close(p2[0]);
            read(p1[0], &helper, sizeof(int));
            //printf("READ : %d\n", helper);
            if (helper > n) {
                write(p2[1], &helper, sizeof(int));
                //printf("WROTE : %d\n", helper);
            }
            else{
                write(p2[1], &n, sizeof(int));
                //printf("WROTE : %d\n", n);
            }
            close(p1[0]);
            close(p2[1]);
            exit(0);
        }
        else
        {
            if(i == 0)
            {
                srand ((unsigned) time (&t)^getpid());
                n = rand () % 501;
                printf("Number : %d, PID : %d \n",n,getpid());
                //printf("WROTE PAI: %d\n", n);
                write(p1[1], &n, sizeof(int));
                close(p1[1]);
                close(p2[1]);
            }
        }
        close(p1[0]);
        close(p1[1]);
        p1[0] = p2[0];
        p1[1] = p2[1];
    }
    
    if(getpid() == ppid)
    {
        wait(NULL);
        read(p1[0], &z , sizeof(int));
        printf("MAX %d\n", z);
    }
    
    return 0;
}
