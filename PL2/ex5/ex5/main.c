//
//  main.c
//  ex5
//
//  Created by Élio Machado on 24/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#define BUFFER_SIZE 256

int main(){
    int fd[2];
    int fu[2];
    char msg [BUFFER_SIZE];
    if (pipe(fu) == -1)
    {
        perror("Error creating pipe up! \n");
        return 1;
    }
    if (pipe(fd) == -1)
    {
        perror("Error creating pipe down! \n");
        return 1;
    }
    pid_t pid;
    if((pid =fork()) == 0){
        printf("String:");
        scanf("%s", msg);
        close(fu[0]);
        write(fu[1], msg, BUFFER_SIZE);
        close(fu[1]);
        close(fd[1]);
        read(fd[0], msg , BUFFER_SIZE);
        printf("Inverted String %s\n", msg);
        close(fd[0]);
    }
    else{
        close (fu[1]);
        read(fu[0], msg , BUFFER_SIZE);
        int i = 0;
        do{
            if((msg[i]) > 96 && (msg[i]) < 122){
                msg[i]=msg[i]-32;
            }
            else if((msg[i]) > 64 && (msg[i]) < 90 ){
                msg[i]=msg[i]+32;
            }
            i++;
        }while(msg[i] != '\0');
        close(fd[0]);
        write(fd[1], msg, BUFFER_SIZE);
        close(fd[1]);
    }
    return 0;
}

