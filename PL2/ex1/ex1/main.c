//
//  main.c
//  ex1
//
//  Created by Élio Machado on 23/03/2019.
//  Copyright © 2019 clarityzzz. All rights reserved.
//

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    int fd[2];
    int ret;
    ret = pipe(fd);
    if(ret == -1){
        printf ("Error creating pipe!");
    }
    pid_t pid;
    if((pid =fork()) == 0){
        int retp;
        close (fd[1]);
        read(fd[0],&retp,sizeof(int));
        printf("pid do pai é : %d \n", retp);
        close (fd[0]);
    }
    else{
        int pid_p = getpid();
        printf("PID_P = %d\n", pid_p);
        close (fd[0]);
        write(fd[1], &pid_p,sizeof(int));
        close (fd[1]);
    }
}
